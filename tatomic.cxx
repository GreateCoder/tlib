#include "tatomic.h"

#ifdef WIN32
#include <windows.h>
#endif

namespace tlib
{
//自增,返回新值
int TAtomic::AtomSelfAdd(void* var)
{
#ifdef WIN32
  return InterlockedIncrement((long*) (var));
#else
  return __sync_add_and_fetch((int*) (var), 1);
#endif
}

//自减,返回新值
int TAtomic::AtomSelfDec(void* var)
{
#ifdef WIN32
  return InterlockedDecrement((long*) (var));
#else
  return __sync_add_and_fetch((int*) (var), -1);
#endif
}

//加一个值,返回旧值
int TAtomic::AtomAdd(void* var, const int value)
{
#ifdef WIN32
  return InterlockedExchangeAdd((long*) (var), value);
#else
  return __sync_fetch_and_add((int*) (var), value);
#endif
}

//减一个值,返回旧值
int TAtomic::AtomDec(void* var, int value)
{
  value = value * -1;
#ifdef WIN32
  return InterlockedExchangeAdd((long*) (var), value);
#else
  return __sync_fetch_and_add((int*) (var), value);
#endif
}

//赋值,windows下返回新值，linux下返回旧值
int TAtomic::AtomSet(void* var, const int value)
{
#ifdef WIN32
  ::InterlockedExchange((long*) (var), (long) (value));
#else
  __sync_lock_test_and_set((int*) (var), value);
#endif
  return value;
}

//取值
int TAtomic::AtomGet(void* var)
{
#ifdef WIN32
  return InterlockedExchangeAdd((long*) (var), 0);
#else
  return __sync_fetch_and_add((int*) (var), 0);
#endif
}

}
