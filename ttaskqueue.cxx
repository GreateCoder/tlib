#include "ttaskqueue.h"
#include "tlogger.h"
namespace tlib
{
TTaskQueue::TTaskQueue()
{

}

TTaskQueue::~TTaskQueue()
{

}

TTask TTaskQueue::Pop()
{
  TAutoLock lock(&m_mutex);
  while (m_tasks.empty())
  {
    m_bNotEmpty.Wait(&m_mutex);
  }
  TTask task;
  if (!m_tasks.empty())
  {
    task = m_tasks.front();
    m_tasks.pop_front();
  }
  return task;
}

void TTaskQueue::Push(TTask tk)
{
  TAutoLock lock(&m_mutex);
  m_tasks.push_back(tk);
  m_bNotEmpty.Notify();
}

int TTaskQueue::Size()
{
  return m_tasks.size();
}
}

