#ifndef _TLOGGER_H_
#define _TLOGGER_H_
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <time.h>
#include <assert.h>
#include "tlock.h"
#include "ttools.h"
#include "tnocopyable.h"
#define log_debug(...)	{ tlib::g_log.WriteLog("DEBUG", __VA_ARGS__);				}
#define log_info(...)	{ tlib::g_log.WriteLog("INFO", __VA_ARGS__);				}
#define log_warn(...)	{ tlib::g_log.WriteLog("WARN", __VA_ARGS__);				}
#define log_error(...)	{ tlib::g_log.WriteLog("ERROR", __VA_ARGS__);				}
#define log_exit(...)   { tlib::g_log.WriteLog("EXIT", __VA_ARGS__);assert(false);	}
namespace tlib
{
class TLogger: TNoCopyable
{
public:
  TLogger();
  virtual ~TLogger();
  void WriteLog(const char* level, const char* fmt, ...);
private:
  FILE* m_pLogFile;
  int m_nLogFileTime;
  char m_pFilePath[256], m_pWriteBuf[10 * 1024];
  TMutex m_mutex;
  void OpenFile(char* command);
};
extern TLogger g_log;
}

#endif

