#include <stdio.h>
#include "main.h"
#include "tlock.h"
#include "tbind.h"
#include "tserver.h"
#include "tthread.h"
#include "tthreadpool.h"
#include "echoserver.h"
using namespace tlib;

int main(int argc, char** argv)
{
  assert(TSocket::EnvInit());

  EchoServer server;
  server.Listen(NULL, 8088);
  server.Start();
  getchar();
  return 0;
}
