#ifndef _TLIB_TTASKQUEUE_H_
#define _TLIB_TTASKQUEUE_H_
#include <deque>
#include "tlock.h"
#include "tthread.h"
namespace tlib
{
class TTaskQueue
{
public:
  TTaskQueue();
  virtual ~TTaskQueue();

  void Push(TTask tk);
  TTask Pop();
  int Size();
private:
  TMutex m_mutex;
  TCondition m_bNotEmpty;
  std::deque<TTask> m_tasks;
};
}

#endif

