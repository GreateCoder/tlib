#include "tserver.h"
#include <stdio.h>
#include <assert.h>
#include <iostream>
namespace tlib
{

TServer::TServer()
{
  m_nSrvFd = TSocket::Create();
}

TServer::~TServer()
{

}

bool TServer::Listen(char* host, int port)
{
  assert(-1 != TSocket::Bind(m_nSrvFd, host, port));
  return true;
}

void TServer::Start()
{
  assert(-1 != TSocket::Listen(m_nSrvFd, 1024));
  if (TSocket::SetReuseAddr(m_nSrvFd, true))
  {
    log_exit("Server SetReuseAddr Error");
  }
  if (TSocket::SetReusePort(m_nSrvFd, true))
  {
    log_exit("Server SetReusePort Error");
  }
  for (int i = 0; i < IO_THREAD_COUNT; i++)
  {
    m_ioThreads[i].Run(bind(&TEventLoop::RunLoop, &m_eventLoop[i], this));
  }
  for (int i = 0; i < BS_THREAD_COUNT; i++)
  {
    m_bsThreads[i].Run(bind(&TServer::RunInBSThread, this, i));
  }

  m_eventLoop[IO_IDX(m_nSrvFd)].AddLoop(m_nSrvFd, Read);
  //用户实现OnMain()方法来保障服务不退出
  OnMain();
}

void TServer::Stop()
{

}

void TServer::RunInBSThread(int th)
{
  log_debug("[%d] -> bs线程[%d]启动..", th, CurrThreadId());
  for (;;)
  {
    TTask task = m_bsQueues[th].Pop();
    task();
  }
}
} //namespace tlib
