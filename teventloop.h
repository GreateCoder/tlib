#ifndef _TEVENTLOOP_H_
#define _TEVENTLOOP_H_
#include <map>
#include <vector>
#include "tnocopyable.h"
#ifdef WIN32
#include <windows.h>
#else
#include <unistd.h>
#include <errno.h>
#include <sys/epoll.h>
#include <sys/socket.h>
#define MAX_EVENTS 1024
#endif

namespace tlib
{
enum EventMonitor
{
  Read = 0, Write
};
class TSocket;
class TServer;
class TClient;
class TEventLoop: TNoCopyable
{
public:
  TEventLoop();
  virtual ~TEventLoop();

  bool AddLoop(int fd, EventMonitor em);
  bool DelLoop(int fd, EventMonitor em);
  void RunLoop(TServer* srv);
  void WakeUp();
  TTaskQueue m_ioQueues;
public:
  int writefd, readfd;
  TServer* pSrv;
  TBufferPool m_bufferPool;
  void SendInIOThread(TClient* cli, std::string msg);
  void SendRemainInIOThread(TClient* cli);
  void CloseInIOThread(TClient* cli);
#ifdef WIN32
  struct timeval m_timeout;
  fd_set m_connSet, m_sendSet, m_recvSet;
  std::map<int, TClient*> m_conns;
#else
  int m_epollfd;
  std::vector<TClient*> m_conns;
  struct epoll_event m_ev, m_events[MAX_EVENTS];
#endif
};
}

#endif
