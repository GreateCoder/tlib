#ifndef _TLIB_TCONNECT_H_
#define _TLIB_TCONNECT_H_
#include "tbuffer.h"
namespace tlib
{
class TServer;
class TClient: TNoCopyable
{
public:
  TClient();
  virtual ~TClient();

  void Send(char* buff, int len);
  bool Read(char* buff, int len);
  bool Remove(int len);

  int GetFD();
  int GetLength();
  int GetSepLength(char* sep, int len);
public:
  int fd;
  bool m_bConn;
  TServer* pSrv;
  TBuffer m_inputBuffer;
  TBuffer m_outputBuffer;
};
}
#endif
