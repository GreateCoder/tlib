#ifndef _TTOOLS_H_
#define _TTOOLS_H_

namespace tlib
{
bool GetExeName(char* exedir, int size);
unsigned int CurrThreadId();
void Sleep(long lMillSecond);
void SetError(char *err, const char *fmt, ...);
} //namespace tlib
#endif
