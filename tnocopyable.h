#ifndef _TNOCOPYABLE_H_
#define _TNOCOPYABLE_H_
namespace tlib
{
class TNoCopyable
{
protected:
  TNoCopyable()
  {
  };
  virtual ~TNoCopyable()
  {
  };
private:
  TNoCopyable(const TNoCopyable&);
  TNoCopyable& operator=(const TNoCopyable&);
};
}

#endif
