#include "echoserver.h"
//std::map<int, int> g_statistics;
int g_fdCount = 0, g_msgCount = 0;
void EchoServer::OnMain()
{
  int mis = 2;
  for (;;)
  {
    tlib::Sleep(mis * 1000);
    log_debug("==>当前连接数: %d   当前处理速度: %d/秒", g_fdCount, g_msgCount/mis);
    g_msgCount = 0;
    //for (std::map<int, int>::iterator it = g_statistics.begin(); it!=g_statistics.end(); it++)
    //{
    //	log_debug("fd[[%d] recv times: %d", it->first, it->second);
    //}
  }
}
//BS线程调用
void EchoServer::OnConn(tlib::TClient* conn)
{
  //g_statistics[conn->fd] = 0;
  g_fdCount++;
  log_debug("Socket[%d] OnConn..", conn->GetFD());
}
//BS线程调用
void EchoServer::OnRecv(tlib::TClient* conn, std::string msg)
{
  //log_debug("Socket[%d] OnRecv..", conn->fd);
  //g_statistics[conn->fd] = g_statistics[conn->fd]++;
  g_msgCount++;
  conn->Send((char*) msg.data(), msg.size());
}
//BS线程调用
void EchoServer::OnClose(tlib::TClient* conn)
{
  g_fdCount--;
  log_debug("Socket[%d] OnClose..", conn->GetFD());
}
//IO线程调用
bool EchoServer::PreRecv(tlib::TClient* conn, std::string &msg)
{
  //log_debug("Socket[%d] PreRecv..", conn->GetFD());
  int blen = conn->GetLength();
  if (blen > 0)
  {
    char buff[BLOCK_SIZE] =
    { 0 };
    if (conn->Read(buff, blen))
    {
      conn->Remove(blen);
      msg = std::string(buff, blen);
      return true;
    }
  }
  return false;
}
