#ifndef _TBuffer_H_
#define _TBuffer_H_
#include <map>
#include <vector>
#include "tlock.h"
namespace tlib
{
#define BLOCK_SIZE	8192
#define BLOCK_COUNT	10000
class TBufferPool
{
public:
  TBufferPool();
  virtual ~TBufferPool();
  // 分配块
  int AllocBlock();
  // 回收块
  void FreeBlock(int blockNo);
  // 获取块指针
  struct STBlock* GetBlock(int blockNo);

private:
  //  TMutex m_mutex;
  struct STBlock* m_pAllBlock;
  std::vector<int> m_nNoUseBlock;
};

class TBuffer
{
public:
  TBuffer();
  virtual ~TBuffer();

  void Bind(TBufferPool* pool);
  /**
   * 从缓存中读取数据
   *     buff    缓存->buff
   *     len     读取长度
   *     del     是否删除数据
   */
  bool Read(char* buff, int len);
  /**
   * 写入缓存
   *     buff    buff->缓存
   *     len     写入长度
   */
  bool Write(char* buff, int len);

  // 从缓存中删除指定长度
  bool Remove(int len);

  // 获取可读取的数据总长度
  int GetLength();

  // 获取第一个指定分隔符的距离, 返回长度不包含分隔符本身
  int GetSepLength(char* sep, int len);

private:
  int m_nReadNo, m_nWriteNo, m_nReadIdx, m_nWriteIdx;
  TBufferPool* m_pBufferPool;
  std::vector<int> m_blocks;
};
}
#endif
