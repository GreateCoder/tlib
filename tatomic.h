#ifndef _TLIB_TATOMIC_H_
#define _TLIB_TATOMIC_H_

namespace tlib
{
class TAtomic
{
private:
  TAtomic();
  virtual
  ~TAtomic();
public:
  static int AtomSelfAdd(void* var);
  static int AtomSelfDec(void* var);
  static int AtomAdd(void* var, const int value);
  static int AtomDec(void* var, int value);
  static int AtomSet(void* var, const int value);
  static int AtomGet(void* var);
};
}

#endif
