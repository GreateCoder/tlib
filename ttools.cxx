#ifdef WIN32
#include <windows.h>
#else
#include <unistd.h>
#include <string.h>
#include <sys/syscall.h>
#endif
#include <stdio.h>
#include <stdarg.h>
#define ERRMSG_LEN 512
namespace tlib
{
bool GetExeName(char* exedir, int size)
{
#ifdef WIN32
  TCHAR *pExepath = (TCHAR*) exedir;
  GetModuleFileName(NULL, pExepath, size);
  char drive[_MAX_DRIVE], dir[_MAX_DIR], fname[_MAX_FNAME], ext[_MAX_EXT];
  _splitpath(pExepath, drive, dir, fname, ext);
  _makepath(pExepath, drive, dir, fname, NULL);
  return true;
#else
  int rval = readlink("/proc/self/exe", exedir, size);
  if (rval == -1)
  {
    strcpy(exedir, "./");
    return false;
  }
  exedir[rval] = '\0';
#endif
  return true;
}

unsigned int CurrThreadId()
{
#ifdef WIN32
  return ::GetCurrentThreadId();
#else
  return ::syscall(__NR_gettid);
#endif
}

void Sleep(long ms)
{
#ifdef WIN32
  ::Sleep(ms);
#else
  ::usleep(ms * 1000);
#endif

}

void SetError(char *err, const char *fmt, ...)
{
  va_list ap;
  if (!err)
    return;
  va_start(ap, fmt);
  vsnprintf(err, ERRMSG_LEN, fmt, ap);
  va_end(ap);
}
} //namespace tlib
