#ifndef _TLIB_TTHREAD_H_
#define _TLIB_TTHREAD_H_
#include "tlock.h"
#include "tfunction.h"
#include "tnocopyable.h"
#ifdef WIN32
#include <windows.h>
#else
#include <pthread.h>
#endif

namespace tlib
{
typedef function<void(void)> TTask;
class TThread: TNoCopyable
{
public:
  TThread();
  virtual ~TThread();

  bool Run(TTask task);
  bool Cannel();
  bool Join();
  bool SetDetach();
  bool UnsetDetach();
  bool GetDetach();
  bool IsActivated();
private:
#ifdef WIN32
  static void GlobleThread(void* param);
#else
  static void* GlobleThread(void* param);
#endif
#ifdef WIN32
  HANDLE m_th;
  DWORD m_thid;
#else
  pthread_t m_th;
#endif
  bool m_bDetached;
  bool m_bActivated;
  TTask m_task;
};
}

#endif
