#ifndef _TLIB_TLOCK_H_
#define _TLIB_TLOCK_H_
#ifdef WIN32
#include <windows.h>
#else
#include <pthread.h>
#endif
#include "tnocopyable.h"
namespace tlib
{
class TMutex: TNoCopyable
{
  friend class TCondition;
public:
  TMutex();
  virtual ~TMutex();

  bool Lock();
  bool Unlock();
  bool Trylock();
private:
#ifdef WIN32
  CRITICAL_SECTION m_mutex;
#else
  pthread_mutex_t m_mutex;
#endif
};

class TCondition: TNoCopyable
{
public:
  TCondition();
  virtual ~TCondition();

  bool Wait(TMutex *pLock);
  bool Notify();
  bool NotifyAll();
private:
#ifdef WIN32
  CONDITION_VARIABLE m_cond;
#else
  pthread_cond_t m_cond;
#endif
};

class TAutoLock: TNoCopyable
{
public:
  TAutoLock(TMutex *pLock);
  virtual ~TAutoLock();
  void Unlock();
private:
  TMutex* m_pMutex;
  bool m_bLocked;
};
}

#endif
