#ifndef _TLIB_TSOCKET_H_
#define _TLIB_TSOCKET_H_
#include "tnocopyable.h"
namespace tlib
{
class TSocket: TNoCopyable
{
private:
  TSocket();
  virtual ~TSocket();
public:
  static bool EnvInit();
  static bool EnvFree();

  static int Send(int fd, char* buff, int len);
  static int Recv(int fd, char* buff, int len);

  static int Create();
  static int Bind(int fd, char* host, int port);
  static int Listen(int fd, int backlog);
  static int Accept(int fd);
  static int Connect(int fd, char* host, int port);
  static void Close(int fd);

  static bool SetNonBlock(int fd, bool on);
  static bool SetTcpDelay(int fd, bool on);
  static bool SetReuseAddr(int fd, bool on);
  static bool SetReusePort(int fd, bool on);
  static bool SetKeepAlive(int fd, bool on);
};
}

#endif
