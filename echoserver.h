#ifndef _ECHOSERVER_H_
#define _ECHOSERVER_H_
#include <string>
#include "tlogger.h"
#include "tsocket.h"
#include "tserver.h"
#include "tclient.h"

class EchoServer: public tlib::TServer
{
public:
  // 主线程调用
  virtual void OnMain();
  // BS线程调用
  virtual void OnConn(tlib::TClient* conn);
  // BS线程调用
  virtual void OnRecv(tlib::TClient* conn, std::string msg);
  // BS线程调用
  virtual void OnClose(tlib::TClient* conn);
  // IO线程调用
  virtual bool PreRecv(tlib::TClient* conn, std::string &msg);
};

#endif
