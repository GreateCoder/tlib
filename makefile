CC=g++
SUFFIX=cxx
CFLAGS=-g -fPIC -Wall -D_REENTRANT -O3

INCLUDE=-I.

SOURCE=\
	echoserver.cxx\
	main.cxx\
	tbuffer.cxx\
	teventloop.cxx\
	tlock.cxx\
	tlogger.cxx\
	tserver.cxx\
	tsocket.cxx\
	ttaskqueue.cxx\
	tthread.cxx\
	tthreadpool.cxx\
	ttools.cxx\
	tclient.cxx\

OBJECTFILE=$(SOURCE:.$(SUFFIX)=.o)

TARGET=xyserver

ALL:$(TARGET) clean

$(TARGET):$(OBJECTFILE)
	@echo "generate $(TARGET)"
	$(CC) -o $@ $^ $(CFLAGS) $(DEPENDENCE) -lpthread

$(OBJECTFILE):%.o:%.$(SUFFIX)
	@echo "complie $@"
	$(CC) -c -o $@ $< $(CFLAGS) $(INCLUDE)

clean:
	rm -f $(OBJECTFILE)
