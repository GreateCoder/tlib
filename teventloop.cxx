#include "tserver.h"
#include "tsocket.h"
#include "teventloop.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>

namespace tlib
{
#ifdef WIN32
int pipe(int fildes[2])
{
  int tcp1, tcp2;
  sockaddr_in name;
  memset(&name, 0, sizeof(name));
  name.sin_family = AF_INET;
  name.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
  int namelen = sizeof(name);
  tcp1 = tcp2 = -1;

  int tcp = ::socket(AF_INET, SOCK_STREAM, 0);
  if (tcp == -1)
  {
    goto clean;
  }
  if (::bind(tcp, (sockaddr*)&name, namelen) == -1)
  {
    goto clean;
  }
  if (::listen(tcp, 5) == -1)
  {
    goto clean;
  }
  if (::getsockname(tcp, (sockaddr*)&name, &namelen) == -1)
  {
    goto clean;
  }
  tcp1 = ::socket(AF_INET, SOCK_STREAM, 0);
  if (tcp1 == -1)
  {
    goto clean;
  }
  if (-1 == ::connect(tcp1, (sockaddr*)&name, namelen))
  {
    goto clean;
  }

  tcp2 = ::accept(tcp, (sockaddr*)&name, &namelen);
  if (tcp2 == -1)
  {
    goto clean;
  }
  if (::closesocket(tcp) == -1)
  {
    goto clean;
  }
  fildes[0] = tcp1;
  fildes[1] = tcp2;
  return 0;
  clean:
  if (tcp != -1)
  {
    ::closesocket(tcp);
  }
  if (tcp2 != -1)
  {
    ::closesocket(tcp2);
  }
  if (tcp1 != -1)
  {
    ::closesocket(tcp1);
  }
  return -1;
}

TEventLoop::TEventLoop()
{
  FD_ZERO(&m_connSet);
  FD_ZERO(&m_sendSet);
  FD_ZERO(&m_recvSet);
  int fd[2];
  if (pipe(fd) < 0)
  log_exit("pipe出错, exit");
  readfd = fd[0];
  writefd = fd[1];
}

TEventLoop::~TEventLoop()
{
}

//IO线程, BS线程都会调用
bool TEventLoop::AddLoop(int fd, EventMonitor em)
{
  if (em == Read)
  {
    FD_SET(fd, &m_recvSet);
    FD_SET(fd, &m_connSet);
  }
  if (em == Write)
  FD_SET(fd, &m_sendSet);
  return true;
}

//IO线程, BS线程都会调用
bool TEventLoop::DelLoop(int fd, EventMonitor em)
{
  if (em == Read)
  {
    FD_CLR(fd, &m_recvSet);
    FD_CLR(fd, &m_sendSet);
    FD_CLR(fd, &m_connSet);
  }
  if (em == Write)
  FD_CLR(fd, &m_sendSet);
  return true;
}

//运行线程: m_ioThreadPool
//调用路径: TServer.Start()->TEventLoop.RunLoop()
void TEventLoop::RunLoop(TServer* srv)
{
  pSrv = srv;
  log_debug("io线程[%d]启动..", CurrThreadId());
  TSocket::SetNonBlock(readfd, true);
  AddLoop(readfd, Read);
  char buff[BLOCK_SIZE];
  fd_set recvFdSet, sendFdSet;
  for (;;)
  {
    FD_ZERO(&recvFdSet);
    FD_ZERO(&sendFdSet);
    recvFdSet = m_recvSet;
    sendFdSet = m_sendSet;
    int ret = ::select(0, &recvFdSet, &sendFdSet, NULL, NULL);
    if (ret > 0)
    {
      for (unsigned int i = 0; i < m_connSet.fd_count; i++)
      {
        if (FD_ISSET(m_connSet.fd_array[i], &recvFdSet))
        {
          if (m_connSet.fd_array[i] == readfd)
          {
            ::recv(readfd, buff, BLOCK_SIZE, NULL);
            for (int x = 0; x < m_ioQueues.Size(); x++)
            {
              (m_ioQueues.Pop())();
            }
          }
          else if (m_connSet.fd_array[i] == pSrv->m_nSrvFd)
          { //监听端口
            int tid = CurrThreadId();
            TClient* cli = new TClient;
            cli->pSrv = pSrv;
            cli->fd = TSocket::Accept(pSrv->m_nSrvFd);
            if (-1 == cli->fd)
            {
              log_error("ACCEPT出错, errno: %d", errno);
              delete cli;
              continue;
            }
            cli->m_inputBuffer.Bind(&m_bufferPool);
            cli->m_outputBuffer.Bind(&m_bufferPool);
            TSocket::SetNonBlock(cli->fd, true);
            //TODO: AddLoop()//考虑线程的情况
            pSrv->m_eventLoop[IO_IDX(cli->fd)].m_conns[cli->fd] = cli;
            pSrv->m_eventLoop[IO_IDX(cli->fd)].AddLoop(cli->fd, Read);
            pSrv->m_bsQueues[BS_IDX(cli->fd)].Push(bind(&TServer::OnConn, pSrv, cli));
          }
          else
          { //接受数据//connSet_.fd_array[i]
            int tid = CurrThreadId();
            TClient* cli = m_conns[m_connSet.fd_array[i]];
            int len = TSocket::Recv(cli->fd, buff, BLOCK_SIZE);
            if (len > 0)
            {
              std::string msg;
              assert(cli->m_inputBuffer.Write(buff, len));
              if (pSrv->PreRecv(cli, msg))
              {
                pSrv->m_bsQueues[BS_IDX(cli->fd)].Push(bind(&TServer::OnRecv, pSrv, cli, msg));
              }
            }
            else if (len == 0)
            { //貌似本来长度为0就是客户端断开, 现在是负数是断开, 这点要注意

            }
            else
            { //客户端断开
              this->CloseInIOThread(cli);
            }
          }
        }
        if (FD_ISSET(m_connSet.fd_array[i], &sendFdSet))
        { //发送数据
          TClient* cli = m_conns[m_connSet.fd_array[i]];
          this->SendRemainInIOThread(cli);
        }
      }
    }
  }
}
#else
TEventLoop::TEventLoop()
{
  m_conns.reserve(10240);
  m_epollfd = ::epoll_create(MAX_EVENTS);
  if (m_epollfd < 0)
    log_exit("epoll_create出错, exit");
  int fd[2];
  if (pipe(fd) < 0)
    log_exit("pipe出错, exit");
  readfd = fd[0];
  writefd = fd[1];
}

TEventLoop::~TEventLoop()
{
  close(m_epollfd);
}

bool TEventLoop::AddLoop(int fd, EventMonitor em)
{
  m_ev.data.fd = fd;
  if (em == Read)
  {
    m_ev.events = EPOLLIN;
    if (-1 == ::epoll_ctl(m_epollfd, EPOLL_CTL_ADD, fd, &m_ev))
      log_error("EPOLL_CTL_ADD出错, errno: %d", errno);
  }
  if (em == Write)
  {
    m_ev.events = EPOLLIN | EPOLLOUT;
    if (-1 == ::epoll_ctl(m_epollfd, EPOLL_CTL_MOD, fd, &m_ev))
      log_error("EPOLL_CTL_MOD出错, errno: %d", errno);
  }
  return true;
}

bool TEventLoop::DelLoop(int fd, EventMonitor em)
{
  m_ev.data.fd = fd;
  if (em == Read)
  {
    if (-1 == ::epoll_ctl(m_epollfd, EPOLL_CTL_DEL, fd, NULL))
      log_error("EPOLL_CTL_DEL出错, errno: %d", errno);
  }
  if (em == Write)
  {
    m_ev.events = EPOLLIN;
    if (-1 == ::epoll_ctl(m_epollfd, EPOLL_CTL_MOD, fd, &m_ev))
      log_error("EPOLL_CTL_MOD出错, errno: %d", errno);
  }
  return true;
}

void TEventLoop::RunLoop(TServer* srv)
{
  pSrv = srv;
  log_debug("io线程[%d]启动..", CurrThreadId());
  TSocket::SetNonBlock(readfd, true);
  AddLoop(readfd, Read);
  char buff[BLOCK_SIZE];
  for (;;)
  {
    int fds = ::epoll_wait(m_epollfd, m_events, MAX_EVENTS, -1);
    for (int i = 0; i < fds; i++)
    {
      if (m_events[i].events & EPOLLIN)
      {
        if (m_events[i].data.fd == readfd)
        {
          ::read(readfd, buff, BLOCK_SIZE);
          for (int x = 0; x < m_ioQueues.Size(); x++)
          {
            (m_ioQueues.Pop())();
          }
        }
        else if (m_events[i].data.fd == pSrv->m_nSrvFd)
        {
          TClient* cli = new TClient;
          cli->pSrv = pSrv;
          cli->fd = TSocket::Accept(pSrv->m_nSrvFd);
          if (-1 == cli->fd)
          {
            log_error("ACCEPT出错, errno: %d", errno);
            delete cli;
            continue;
          }
          cli->m_inputBuffer.Bind(&m_bufferPool);
          cli->m_outputBuffer.Bind(&m_bufferPool);
          TSocket::SetNonBlock(cli->fd, true);
          pSrv->m_eventLoop[IO_IDX(cli->fd)].m_conns[cli->fd] = cli;
          pSrv->m_eventLoop[IO_IDX(cli->fd)].AddLoop(cli->fd, Read);
          pSrv->m_bsQueues[BS_IDX(cli->fd)].Push(bind(&TServer::OnConn, pSrv, cli));
        }
        else
        {
          TClient* cli = m_conns[m_events[i].data.fd];
          int len = TSocket::Recv(cli->fd, buff, BLOCK_SIZE);
          if (len > 0)
          {
            std::string msg;
            assert(cli->m_inputBuffer.Write(buff, len));
            if (pSrv->PreRecv(cli, msg))
            {
              pSrv->m_bsQueues[BS_IDX(cli->fd)].Push(bind(&TServer::OnRecv, pSrv, cli, msg));
            }
          }
          else if (len == 0)
          { //貌似本来长度为0就是客户端断开, 现在是负数是断开, 这点要注意

          }
          else
          { //客户端断开
            this->CloseInIOThread(cli);
          }
        }
      }
      if (m_events[i].events & EPOLLOUT)
      {
        TClient* cli = m_conns[m_events[i].data.fd];
        this->SendRemainInIOThread(cli);
      }
    }
  }
}

#endif

void TEventLoop::WakeUp()
{
  char chr = '1';
#ifdef WIN32
  ::send(writefd, &chr, 1, NULL);
#else
  ::write(writefd, &chr, 1);
#endif
}

void TEventLoop::SendInIOThread(TClient* cli, std::string msg)
{
  int len = msg.size();
  char* buff = (char*) msg.data();
  int ableLen = cli->m_outputBuffer.GetLength();
  if (0 == ableLen)
  {
    int realLen = TSocket::Send(cli->GetFD(), buff, len);
    if (realLen > 0 && realLen < len)
    {
      log_debug("sock[%d]期望发送[%d]字节, 实际发送[%d]字节", cli->GetFD(), len, realLen);
      cli->m_outputBuffer.Write(&buff[realLen], len - realLen);
      this->AddLoop(cli->GetFD(), Write);
    }
    if (-1 == realLen)
    {
//      log_info("发送导致关闭连接[%d]", cli->fd);
      this->CloseInIOThread(cli);
    }
  }
}

void TEventLoop::SendRemainInIOThread(TClient* cli)
{
  int len = cli->m_outputBuffer.GetLength();
  if (len == 0)
    this->DelLoop(cli->GetFD(), Write);
  if (len > 0)
  {
    log_debug("sock[%d]开始发送缓存中的[%d]字节", cli->GetFD(), len);
    char* buff = (char*) malloc(len);
    assert(cli->m_outputBuffer.Read(buff, len));
    int realLen = TSocket::Send(cli->GetFD(), buff, len);
    if (realLen > 0)
    {
      cli->m_outputBuffer.Remove(realLen);
    }
    if (-1 == realLen)
    {
//      log_info("发送导致关闭连接[%d]", cli->fd);
      this->CloseInIOThread(cli);
    }
    free(buff);
  }
}

void TEventLoop::CloseInIOThread(TClient* cli)
{
  if (true == cli->m_bConn)
  {
    cli->m_bConn = false;
    this->DelLoop(cli->fd, Read);
    TSocket::Close(cli->fd);
    cli->m_inputBuffer.Remove(cli->m_inputBuffer.GetLength());
    cli->m_outputBuffer.Remove(cli->m_outputBuffer.GetLength());
    pSrv->m_bsQueues[BS_IDX(cli->fd)].Push(bind(&TServer::OnClose, pSrv, cli));
  }
}
} //namespace tlib
