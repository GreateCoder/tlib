#include <stdlib.h>
#include <assert.h>
#include "tclient.h"
#include "tlogger.h"
#include "tsocket.h"
#include "tserver.h"
namespace tlib
{
TClient::TClient() :
    fd(-1), m_bConn(true)
{

}

TClient::~TClient()
{

}

void TClient::Send(char* buff, int len)
{
  std::string msg(buff, len);
  TEventLoop* loop = &(pSrv->m_eventLoop[IO_IDX(fd)]);
  loop->m_ioQueues.Push(bind(&TEventLoop::SendInIOThread, loop, this, msg));
  loop->WakeUp();
}

bool TClient::Read(char* buff, int len)
{
  return m_inputBuffer.Read(buff, len);
}

bool TClient::Remove(int len)
{
  return m_inputBuffer.Remove(len);
}

int TClient::GetFD()
{
  return fd;
}

int TClient::GetLength()
{
  return m_inputBuffer.GetLength();
}

int TClient::GetSepLength(char* sep, int len)
{
  return m_inputBuffer.GetSepLength(sep, len);
}

} //namespace tlib

