#ifndef _TSERVER_H_
#define _TSERVER_H_
#include <map>
#include <string>
#include <vector>
#include "tbind.h"
#include "tlogger.h"
#include "tsocket.h"
#include "tbuffer.h"
#include "tclient.h"
#include "ttaskqueue.h"
#include "teventloop.h"
#include "tthreadpool.h"
#include "tnocopyable.h"
namespace tlib
{
#define IO_THREAD_COUNT 1
#define BS_THREAD_COUNT 4
#ifdef WIN32
#define IO_IDX(FD) FD/4%IO_THREAD_COUNT
#define BS_IDX(FD) FD/4%BS_THREAD_COUNT
#else
#define IO_IDX(FD) FD%IO_THREAD_COUNT
#define BS_IDX(FD) FD%BS_THREAD_COUNT
#endif

class TServer: TNoCopyable
{
  friend class TEventLoop;
public:
  TServer();
  virtual ~TServer();

  bool Listen(char* host, int port);
  void Start();
  void Stop();

  // 主线程调用, 用户实现, OnMain方法死循环来确保服务不退出
  virtual void OnMain() = 0;
  // BS线程调用, 用户实现, 可操作业务
  virtual void OnConn(TClient* conn) = 0;
  // BS线程调用, 用户实现, 可操作业务
  virtual void OnRecv(TClient* conn, std::string msg) = 0;
  // BS线程调用, 用户实现, 可操作业务
  virtual void OnClose(TClient* conn) = 0;
  // IO线程调用, 用户实现, 不可操作业务, 只做简单的报文拼装操作, 一旦拼装好一个完整的报文, 请返回true, 否则, 返回false
  virtual bool PreRecv(TClient* conn, std::string &msg) = 0;
public:
  int m_nSrvFd;
  TThread m_ioThreads[IO_THREAD_COUNT];
  TEventLoop m_eventLoop[IO_THREAD_COUNT];
  TThread m_bsThreads[BS_THREAD_COUNT];
  TTaskQueue m_bsQueues[BS_THREAD_COUNT];

  void RunInBSThread(int th);
};
} //namespace tlib

#endif
