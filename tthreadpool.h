#ifndef _TTHREADPOOL_H_
#define _TTHREADPOOL_H_
#include <deque>
#include <vector>
#include "tlock.h"
#include "tthread.h"
#include "tnocopyable.h"

namespace tlib
{
class TThreadPool: TNoCopyable
{
public:
  TThreadPool();
  virtual ~TThreadPool();

  void Start(int numThreads);
  void Stop();
  void Run(TTask tk);
  TTask Take();
private:
  bool m_bRunning;
  TMutex m_mutex;
  TCondition m_bNotEmpty;
  std::deque<TTask> m_tasks;
  std::vector<TThread*> m_threads;

  void runInThread();
};
}

#endif
